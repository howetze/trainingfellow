package registrierung

type Registrierung struct {
	Firstname             string
	Lastname              string
	Email                 string
	First                 string
	Schulungscode         string
	Datum                 string
	DatenschutzAkzeptiert string
	//Adresse etc. nicht dargestellt
}
